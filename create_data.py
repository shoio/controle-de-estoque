import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "projeto.settings")
django.setup()


import string
import timeit
from random import random, choice, randint
from produto.models import Produto


class Utils:
	"""Métodos genéricos"""
	@staticmethod
	def gen_digits(max_length):
		return str(''.join(choice(string.digits) for i in range(max_length)))


class ProdutoClass:

	@staticmethod
	def criar_produtos(produtos):
		Produto.objects.all().delete()
		aux = []

		for produto in produtos:
			data = dict(
				produto=produto,
				importado=choice((True, False)),
				ncm=Utils.gen_digits(8),
				preco=random()*randint(10, 50),
				estoque=randint(10, 200),
			)
			obj = Produto(**data)
			aux.append(obj)
		Produto.objects.bulk_create(aux)


produtos = (
	'Apontador',
	'Caderno 100 folhas',
	'Caderno capa dura',
	'Caneta azul',
	'Caneta preta',
	'Caneta vermelha',
	'Durex',
	'Giz de cera',
	'Lapiseira 0,3',
	'Lapiseira 0,5',
	'Lapiseira 0,7',
	'Lapis de cor 24 cores',
	'Lapis',
	'Papel A4',
	'Pasta elástica',
	'Tesoura',
)

tic = timeit.default_timer()

ProdutoClass.criar_produtos(produtos) 

toc = timeit.default_timer()

print("Tempo: ",toc - tic)